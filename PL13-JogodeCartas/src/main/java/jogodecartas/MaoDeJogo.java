package jogodecartas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Representa uma mão de jogo.
 *
 * @author ISEP-DEI-PPROG
 */
public class MaoDeJogo {

     /**
     * A mão de jogo.
     */
    private List<Carta> maoDeJogo;
    
    /**
     * Constrói uma instância de {@code MaoDeJogo} recebendo um conjunto de
     * cartas.
     * 
     * @param mao o conjunto de cartas.
     */
    public MaoDeJogo(List<Carta> mao) {
        maoDeJogo = new ArrayList(mao);
    }

    /**
     * Constrói uma instância de {@code MaoDeJogo} vazia, ou seja, sem cartas.
     */
    public MaoDeJogo() {
        maoDeJogo = new ArrayList<>();
    }
    
    /**
     * Constrói uma instância de {@code MaoDeJogo} com as mesmas caraterísticas
     * da mão de jogo recebida por parâmetro.
     * 
     * @param outraMaoDeJogo a mão de jogo com as caraterísticas a copiar.
     */
    public MaoDeJogo(MaoDeJogo outraMaoDeJogo) {
        maoDeJogo = new ArrayList<Carta>();
        for(Carta c : outraMaoDeJogo.maoDeJogo) {
            maoDeJogo.add(new Carta(c));
        }
    }

    /**
     * Devolve a mão de jogo.
     *
     * @return mão de jogo.
     */
    public List<Carta> getMaoDeJogo() {
        return new ArrayList<Carta>(maoDeJogo);
    }
    
    /**
     * Devolve a descrição textual da mão de jogo (uma carta por linha).
     * 
     * @return caraterísticas das cartas da mão de jogo.
     */
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (Carta c : maoDeJogo) {
            s.append(c.toString());
            s.append("\n");
        }
        return s.toString();
    }

    /**
     * Devolve um conjunto com os vários tipos de carta (duque, terno, etc.)
     * existentes na mão de jogo.
     * 
     * @return {@code List} com os tipos de carta existentes na mão de jogo.
     */
    public List<Carta.Tipo> tiposDeCarta() {
        List<Carta.Tipo> cartasNaMao = new ArrayList<>();
        for (int i = 0; i < getMaoDeJogo().size(); i++) {
            cartasNaMao.add(getMaoDeJogo().get(i).getTipo());
        }
        return cartasNaMao;
    }

    /**
     * Determina quantas cartas de um dado tipo existem na mão de jogo.
     * 
     * @param tipo o tipo de carta.
     * @return quantidade de cartas do tipo, que existem na mão de jogo.
     */
    public int contarCartasDoTipo(Carta.Tipo tipo) {
        int soma = 0;
        for (int i = 0; i < getMaoDeJogo().size(); i++) {
            if (getMaoDeJogo().get(i).isSameType(tipo)) {
                soma++;
            }
        }
        return soma;
    }

    /**
     * Ordena a mão de jogo por naipe e, dentro do mesmo naipe, ordena por tipo.
     */
    public void ordenar() {
        // A classe Carta deve implementar a interface Comparable, devendo
        // implementar especificamente Comparable<Carta>
        Collections.sort(maoDeJogo);
    }
    
    public boolean equals(MaoDeJogo otaMao) {
        if (this == otaMao) {
            return true;
        }
        if (otaMao == null) {
            return false;
        }
     
        ArrayList<Carta> ricardo = new ArrayList<Carta>();
        ArrayList<Carta> isaac = new ArrayList<Carta>();
        ricardo = (ArrayList<Carta>) getMaoDeJogo();
        isaac = (ArrayList<Carta>) otaMao.getMaoDeJogo();
        if (ricardo.size() != isaac.size()) {
            return false;
        }
        Collections.sort(isaac);
        Collections.sort(ricardo);
        for (int i = 0; i < ricardo.size(); i++) {
            if (!ricardo.get(i).equals(isaac.get(i))) {
                return false;
            }
        }
        return true;
    }
}
