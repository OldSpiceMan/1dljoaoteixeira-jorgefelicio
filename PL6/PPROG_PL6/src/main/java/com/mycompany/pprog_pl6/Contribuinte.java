/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pprog_pl6;

/**
 *
 * @author Jorge Felicio
 */
public abstract class Contribuinte {

    private String nome;

    private final String NOME_POR_OMISSAO = "...";

    private String morada;

    private final String MORADA_POR_OMISSAO = "...";

    int outrosRendimentos;

    private int OUTROS_RENDIMENTOS_POR_OMISSAO = 0;
    
    private float TaxaOR;
    
    float imposto;

    Contribuinte(String nome, String morada, int outrosRendimentos) {
        this.nome = nome;
        this.morada = morada;
        this.outrosRendimentos = outrosRendimentos;
    }

    Contribuinte() {
        this.nome = NOME_POR_OMISSAO;
        this.morada = MORADA_POR_OMISSAO;
        this.outrosRendimentos = OUTROS_RENDIMENTOS_POR_OMISSAO;
    }

    public String getNome() {
        return nome;
    }
    
    public String getMorada(){
        return morada;
    }
    
    public abstract int getOutrosRendimentos();
    
    public void setNome(String nome){
        this.nome=nome;
    }
    
    public void setMorada(String morada){
        this.morada=morada;
    }
    
    public float getTaxaOR(){
        return Desempregado.TaxaOR;
    }
    
    public void setTaxaOR(float TaxaOR){
        this.TaxaOR = TaxaOR;
    }
    
    public void setOutrosRendimentos(int outrosRendimentos){
        this.outrosRendimentos=outrosRendimentos;
    }
    
    public String toString(){
        return String.format("O nome do Contribuinte é %s, a sua morada é %s,o valor dos seus outros Rendimentos são %s",nome,morada,outrosRendimentos);
    }
    
    public float getImposto(){
        return imposto;
    }
    
    public abstract float calculoImposto(int outrosRendimentos,int rendimentoTrabalho);

    public abstract int getRendimentoTrabalho();
    
}
