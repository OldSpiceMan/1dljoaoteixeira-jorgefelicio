/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pprog_pl6;

/**
 *
 * @author Jorge Felicio
 */
public class TrabContaPropria extends TrabalhadorComRT {

    private static float TaxaRT = (float) 0.03;

    private static float TaxaOR;

    private String profissao;

    private String PROFISSAO_POR_OMISSAO = "...";

    TrabContaPropria(String nome, String morada, String profissao, int rendimentoTrabalho, int outrosRendimentos) {
        super(nome, morada, rendimentoTrabalho, outrosRendimentos);
        this.profissao = profissao;
    }

    TrabContaPropria() {
        super();
        this.profissao = PROFISSAO_POR_OMISSAO;
    }

    public String getProfissao() {
        return profissao;
    }

    public void setProfissao(String profissao) {
        this.profissao = profissao;
    }

    public void setTaxaOR(float TaxaOR) {
        this.TaxaOR = TaxaOR;
    }

    public float getTaxaOR() {
        return TaxaOR;
    }

    public void verificacao() {
        if (super.verificacao(getOutrosRendimentos(), super.limOR) == true) {
            setTaxaOR((float) 0.02);
        } else {
            setTaxaOR((float) 0.05);
            System.out.println(TaxaOR);
        }
    }

    public String getNome() {
        return super.getNome();
    }

    public float calculoImposto(int outrosRendimentos, int rendimentoTrabalho) {
        float imposto = (float) rendimentoTrabalho * this.TaxaRT;
        float imposto2 = (float) outrosRendimentos * this.TaxaOR;
        return imposto + imposto2;
    }

    @Override
    public int getOutrosRendimentos() {
        return outrosRendimentos;
    }

    public String toString() {
        return String.format(super.toString() + ",a sua TaxaRT é %s, a sua TaxaOR é %s ,sua profissao é %s "
                + "e os seus impostos são %s", TrabContaPropria.TaxaRT, getTaxaOR(), profissao, calculoImposto(getOutrosRendimentos(), super.getRendimentoTrabalho()));
    }

}
