/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pprog_pl6;

/**
 *
 * @author Jorge Felicio
 */
public class Desempregado extends Contribuinte {

    static float TaxaOR = (float) 0.02;

    private int mesesParagem;

    private final int MESES_PARAGEM_POR_OMISSAO = 0;
    
    private int rendimentoTrabalho;
    
    private int TaxaRT = 0;
    
    

    Desempregado(String nome, String morada, int outrosRendimentos, int mesesParagem) {
        super(nome, morada, outrosRendimentos);
        this.rendimentoTrabalho = 0;
        this.mesesParagem = mesesParagem;
    }

    Desempregado() {
        super();
        this.rendimentoTrabalho = 0;
        this.mesesParagem = MESES_PARAGEM_POR_OMISSAO;
    }

    public int getMesesParagem() {
        return mesesParagem;
    }
    
    @Override
    public int getOutrosRendimentos(){
        return outrosRendimentos;
    }
     
    public int getRendimentoTrabalho() {
        return rendimentoTrabalho;
    }

    @Override
    public float getTaxaOR() {
        return TaxaOR;
    }

    @Override
    public void setTaxaOR(float TaxaOR) {
        Desempregado.TaxaOR = TaxaOR;
    }

    public void setMesesParagem(int mesesParagem) {
        this.mesesParagem = mesesParagem;
    }

    @Override
    public String getNome() {
        return super.getNome();
    }

    @Override
    public String toString() {
        return super.toString() + String.format("e os seus impostos são %s", calculoImposto(getOutrosRendimentos(),this.rendimentoTrabalho));
    }

    public float calculoImposto(int outrosRendimentos, int rendimentoTrabalho) {
        float imposto = (float) rendimentoTrabalho * this.TaxaRT;
        float imposto2 = (float) outrosRendimentos * this.TaxaOR;
        return imposto + imposto2;
    }

}
