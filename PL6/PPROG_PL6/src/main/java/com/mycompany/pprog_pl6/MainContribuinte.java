/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pprog_pl6;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jorge Felicio
 */
public class MainContribuinte {

    public static void main(String[] args) {
        TrabContaPropria Jorge = new TrabContaPropria("Jorge", "rua morada", "Estudante", 500000, 609000);
        TrabContaPropria Joao = new TrabContaPropria("Joao", "rua morada2", "Estudante", 800, 420);
        TrabContaOutrem Wiener = new TrabContaOutrem();
        TrabContaOutrem Arnold = new TrabContaOutrem("Arnold", "rua Arnold", "Arnold inc", 70000, 90000);
        Reformado sed = new Reformado("Sed", "Rua sed", 400, 600);
        Reformado sed4 = new Reformado();
        Desempregado Ricardo = new Desempregado("Ricardo", "Rua comunismo", 420, 5);
        Desempregado Isaac = new Desempregado();
        Jorge.verificacao();
        Joao.verificacao();
        Wiener.verificacao();
        Arnold.verificacao();
        List<Contribuinte> Contr = new ArrayList<>();
        Contr.add(Jorge);
        Contr.add(Joao);
        Contr.add(Wiener);
        Contr.add(Arnold);
        Contr.add(sed);
        Contr.add(sed4);
        Contr.add(Ricardo);
        Contr.add(Isaac);
        for (int i=0;i < Contr.size();i++) {

            System.out.println(Contr.get(i).toString());
            if ((Contr.get(i) instanceof Contribuinte) && (!(Contr.get(i) instanceof TrabalhadorComRT))) {
                System.out.println(Contr.get(i).getNome() + " e o imposto é " + Contr.get(i).calculoImposto(Contr.get(i).getOutrosRendimentos(),
                        Contr.get(i).getRendimentoTrabalho()));
                Contr.get(i).setTaxaOR((float) 0.04);
                System.out.println(Contr.get(i).getNome() + " e o imposto é " + Contr.get(i).calculoImposto(Contr.get(i).getOutrosRendimentos(),
                        Contr.get(i).getRendimentoTrabalho()));
            }

        }
    }
}
