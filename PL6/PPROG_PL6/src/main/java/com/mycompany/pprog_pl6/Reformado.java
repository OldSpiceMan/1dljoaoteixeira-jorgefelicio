/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pprog_pl6;

/**
 *
 * @author Jorge Felicio
 */
public class Reformado extends TrabalhadorComRT {

    private static final float TaxaRT = (float) 0.01;

    private static final float TaxaOR = (float) 0.03;
    

    Reformado(String nome, String morada, int rendimentoTrabalho, int outrosRendimentos) {
        super(nome, morada, rendimentoTrabalho, outrosRendimentos);
    }

    Reformado() {
        super();
    }

    public String getNome() {
        return super.getNome();
    }
    
     public int getOutrosRendimentos(){
        return outrosRendimentos;
    }

    public String toString() {
        return String.format(super.toString() + ",a sua TaxaRT é %s , a "
                + "sua TaxaOR é %s os seus impostos são %s", TaxaRT, TaxaOR, calculoImposto(getOutrosRendimentos(),super.getRendimentoTrabalho()));
    }

    public float calculoImposto(int outrosRendimentos, int rendimentoTrabalho) {
        float imposto = (float) rendimentoTrabalho * this.TaxaRT;
        float imposto2 = (float) outrosRendimentos * this.TaxaOR;
        return imposto + imposto2;

    }

}
