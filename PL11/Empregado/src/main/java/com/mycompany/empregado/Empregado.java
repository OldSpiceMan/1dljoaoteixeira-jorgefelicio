/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.empregado;

import com.mycompany.utilitarios.Data;
import com.mycompany.utilitarios.Tempo;

/**
 *
 * @author Jorge Felicio
 */
public class Empregado {

    private String primeiroNome;
    private String PRIMEIRO_NOME_POR_OMISSAO = "...";
    private String ultimoNome;
    private String ULTIMO_NOME_POR_OMISSAO = "...";
    private Data dataContrato;
    private Tempo horaEntrada;
    private Tempo horaSaida;

    public Empregado(String primeiroNome, String ultimoNome, Data dataContrato, Tempo horaEntrada,
            Tempo horaSaida) {
        this.primeiroNome = primeiroNome;
        this.ultimoNome = ultimoNome;
        this.dataContrato = dataContrato;
        this.horaEntrada = horaEntrada;
        this.horaSaida = horaSaida;
    }
    
     public Empregado() {
        this.primeiroNome = PRIMEIRO_NOME_POR_OMISSAO;
        this.ultimoNome = ULTIMO_NOME_POR_OMISSAO;
        dataContrato = new Data();
        horaEntrada = new Tempo();
        horaSaida = new Tempo();
    }

    public int calculoHorasSemanal() {
        int horasSemanal = horaSaida.getHoras() - horaEntrada.getHoras();
        horasSemanal = horasSemanal * 5;
        return horasSemanal;
    }
    

    public Data getDataContrato() {
        return dataContrato;
    }
    
    public Tempo getHoraEntrada(){
        return horaEntrada;
    }
    
    public Tempo getHoraSaida(){
        return horaSaida;
    }
    
    public void setDataContrato(Data data){
        this.dataContrato = data;
    }
    
    public void setHoraEntrada(Tempo horaEntrada){
        this.horaEntrada = horaEntrada;
    }
    
    public void setHoraSaida(Tempo horaSaida){
        this.horaSaida = horaSaida;
    }
    
    public String toString(){
        return "Data:" + dataContrato.toString() + ", hora Entrada:" + horaEntrada.toString() +
                ", Hora Saida:" + horaSaida.toString() + "Nome completo:" + this.primeiroNome + this.ultimoNome;
    }
    
    public String getNome(){
        return this.primeiroNome + " " + this.ultimoNome;
    }
}
