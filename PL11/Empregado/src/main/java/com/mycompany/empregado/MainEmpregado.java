/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.empregado;

import com.mycompany.utilitarios.Data;
import com.mycompany.utilitarios.Tempo;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Jorge Felicio
 */
public class MainEmpregado {
    public static void main(String[] args) {
        Data dataAtual = new Data();
        dataAtual = dataAtual.dataAtual();
        System.out.println(dataAtual.toString());
        Tempo horaEntrada = new Tempo(12,49,50);
        Tempo horaSaida = new Tempo(14,23,45);
        System.out.println(horaEntrada.toString());
        System.out.println(horaSaida.toString());
        Empregado Jeremias = new Empregado("Jeremias", "Jeremias", dataAtual, horaEntrada, horaSaida);
        Empregado wiener = new Empregado("Wiener","Schnitzel",dataAtual,horaEntrada,horaSaida);
        System.out.println(Jeremias.getDataContrato().equals(wiener.getDataContrato()));
        System.out.println(Jeremias.getHoraEntrada().equals(wiener.getHoraEntrada()));
        System.out.println(Jeremias.getHoraSaida().equals(wiener.getHoraSaida()));
        dataAtual = new Data(2010,10,12);
        horaEntrada = new Tempo(13,15,16);
        horaSaida = new Tempo(20,21,21);
        System.out.println(dataAtual.toString());
        System.out.println(horaEntrada.toString());
        System.out.println(horaSaida.toString());
        Data dataAtual2 = new Data(2009,12,16);
        Tempo horaEntrada2 = new Tempo(21,13,15);
        Tempo horaSaida2 = new Tempo(23,12,34);
        wiener.setDataContrato(dataAtual2);
        wiener.setHoraEntrada(horaEntrada2);
        wiener.setHoraSaida(horaSaida2);
        List<Empregado> Empregados = new ArrayList<>();
        Empregados.add(Jeremias);
        Empregados.add(wiener);
        for(int i = 0; i<Empregados.size();i++){
            System.out.println(Empregados.get(i).toString());
        }
        for(int i = 0; i<Empregados.size();i++){
            System.out.println("Nome" + Empregados.get(i).getNome()+ ", Horas de Trabalho:"+
                    Empregados.get(i).calculoHorasSemanal()+ ", Antiguidade dias: " + 
                    Empregados.get(i).getDataContrato().diferenca(dataAtual.dataAtual()));
        }
    }
}
