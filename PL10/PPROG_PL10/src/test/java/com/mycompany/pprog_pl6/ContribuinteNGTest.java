/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pprog_pl6;

import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author Jorge Felicio
 */
public class ContribuinteNGTest {

    /**
     * Test of calculoImposto method, of class Contribuinte.
     */
    @Test
    public void testCalculoImposto() {
        System.out.println("calculoImposto");
        int outrosRendimentos = 0;
        int rendimentoTrabalho = 0;
        Contribuinte Lito = new Desempregado("Lito Vidal", "Lisboa", 0, 0);
        Contribuinte Valter = new Reformado("Valter Cubilhas", "Olival", 0, 0);
        Contribuinte Ana = new TrabContaOutrem("Ana", "Ovar", "CMM", 0, 0);
        Contribuinte Mario = new TrabContaPropria("Mário", "Guarda", "Eletricista", 0, 0);
        float expResult = 0.0F;
        float result = Lito.calculoImposto(outrosRendimentos, rendimentoTrabalho);
        float result2 = Valter.calculoImposto(outrosRendimentos, rendimentoTrabalho);
        float result3 = Ana.calculoImposto(outrosRendimentos, rendimentoTrabalho);
        float result4 = Mario.calculoImposto(outrosRendimentos, rendimentoTrabalho);
        assertEquals(result, expResult, 0.0);
        assertEquals(result2, expResult, 0.0);
        assertEquals(result3, expResult, 0.0);
        assertEquals(result4, expResult, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        
    }

}
