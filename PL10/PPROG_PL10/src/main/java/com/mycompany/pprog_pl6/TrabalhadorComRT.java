/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pprog_pl6;

/**
 *
 * @author Jorge Felicio
 */
abstract class TrabalhadorComRT extends Contribuinte {

    private int rendimentoTrabalho;

    private int RENDIMENTO_TRABALHO_POR_OMISSAO = 0;

    int limRT = 30000;

    int limOR = 50000;

    TrabalhadorComRT(String nome, String morada, int rendimentoTrabalho, int outrosRendimentos) {
        super(nome, morada, outrosRendimentos);
        this.rendimentoTrabalho = rendimentoTrabalho;
    }

    TrabalhadorComRT() {
        super();
        this.rendimentoTrabalho = RENDIMENTO_TRABALHO_POR_OMISSAO;
    }

    public int getRendimentoTrabalho() {
        return rendimentoTrabalho;
    }

    public void setRendimentoTrabalho(int rendimentoTrabalho) {
        this.rendimentoTrabalho = rendimentoTrabalho;
    }

    public String toString() {
        return String.format(super.toString() + ",o seu rendimento de Trabalho é %s", rendimentoTrabalho);
    }

    public boolean verificacao(int rendimento, int limiteRendimento) {
        if (rendimento <= limRT) {
            return true;
        } else {
            return false;
        }
    }

    public String getNome() {
        return super.getNome();
    }

    public abstract float calculoImposto(int outrosRendimentos, int rendimentoTrabalho);

    public boolean equals(Object outroObject) {
        if (outroObject != null && outroObject.getClass() == getClass()) {
            return super.equals(outroObject) && rendimentoTrabalho == ((TrabalhadorComRT) outroObject).rendimentoTrabalho;
        }
        return false;

    }
}
