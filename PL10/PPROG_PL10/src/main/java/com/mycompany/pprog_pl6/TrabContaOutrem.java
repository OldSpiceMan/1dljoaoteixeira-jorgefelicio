/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pprog_pl6;

/**
 *
 * @author Jorge Felicio
 */
public class TrabContaOutrem extends TrabalhadorComRT {

    private static float TaxaRT;

    private static float TaxaOR = (float) 0.02;

    private String empresa;

    private String EMPRESA_POR_OMISSAO = "...";

    TrabContaOutrem(String nome, String morada, String empresa, int rendimentoTrabalho, int outrosRendimentos) {
        super(nome, morada, rendimentoTrabalho, outrosRendimentos);
        this.empresa = empresa;

    }

    TrabContaOutrem() {
        super();
        this.empresa = EMPRESA_POR_OMISSAO;

    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public void verificacao() {
        if (super.verificacao(super.getRendimentoTrabalho(), super.limRT) == true) {
            this.TaxaRT = (float) 0.01;
        } else {
            this.TaxaRT = (float) 0.02;
        }
    }

    public String getNome() {
        return super.getNome();
    }

    public int getOutrosRendimentos() {
        return outrosRendimentos;
    }

    public String toString() {
        return String.format(super.toString() + ",a sua TaxaRT é %s , a sua TaxaOR é %s , a sua empresa é %s e os seus "
                + "impostos são %s", TaxaRT, TaxaOR, empresa, calculoImposto(getOutrosRendimentos(), super.getRendimentoTrabalho()));
    }

    public float calculoImposto(int outrosRendimentos, int rendimentoTrabalho) {
        float imposto = (float) rendimentoTrabalho * this.TaxaRT;
        float imposto2 = (float) outrosRendimentos * this.TaxaOR;
        return imposto + imposto2;
    }

    public boolean equals(Object outroObject) {
        if (outroObject instanceof TrabContaOutrem) {
            return super.equals(outroObject) && empresa.equals(((TrabContaOutrem)outroObject).getEmpresa());
        }
        return false;
    }

}
