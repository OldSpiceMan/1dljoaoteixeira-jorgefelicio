/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pprog_pl6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Jorge Felicio
 */
public class MainContribuinte {

    public static void main(String[] args) {
        List<Contribuinte> contribuintes = new ArrayList<>();
        contribuintes.add(new Desempregado("Lito Vidal", "Lisboa", 230, 1));
        contribuintes.add(new Reformado("Valter Cubilhas", "Olival", 600, 400));
        contribuintes.add(new Desempregado("Paulo Santos", "Braga", 150, 4));
        contribuintes.add(new TrabContaOutrem("Ana", "Ovar", "CMM", 1800, 300));
        contribuintes.add(new Reformado("Jaime Magalhães", "Porto", 1500, 100));
        contribuintes.add(new TrabContaOutrem("Salvio", "Seixal", "SSB", 8000, 1000));
        contribuintes.add(new TrabContaPropria("Mário", "Guarda", "Eletricista", 1500, 300));
        contribuintes.add(new Reformado("Aníbal Silva", "Coimbra", 1000, 500));
        contribuintes.add(new TrabContaOutrem("João", "Lisboa", "CML", 800, 100));
        contribuintes.add(new TrabContaPropria("Carla", "Porto", "Advogado", 15000, 1000));
        contribuintes.add(new Desempregado("Maria Sá", "Afife", 230, 8));
        for (int i = 0; i < contribuintes.size(); i++) {
            System.out.println(contribuintes.get(i).toString());
        }

        Contribuinte Lito = contribuintes.get(0);
        contribuintes.remove(Lito);
        contribuintes.add(Lito);
        System.out.println("");
        for (int i = 0; i < contribuintes.size(); i++) {
            System.out.println(contribuintes.get(i).toString());
        }

        System.out.println("");
        Lito = contribuintes.get(10);
        System.out.println(Lito.equals(Lito));
        contribuintes.add(new Reformado("Valter Cubilsdfdshas", "Olival", 600, 400));
        System.out.println(contribuintes.get(11).equals(contribuintes.get(0)));
        System.out.println("");

        System.out.println(contribuintes.get(0).equals(contribuintes.get(1)));
        for (int i = 0; i < contribuintes.size(); i++) {
            System.out.println(contribuintes.get(i).toString());
        }
        Comparator<Contribuinte> comparatorTipo = new Comparator<Contribuinte>() {
            @Override
            public int compare(Contribuinte o1, Contribuinte o2) {
                String tipo1 = o1.getClass().getName();
                String tipo2 = o2.getClass().getName();
                int result = tipo1.compareTo(tipo2);
                if(result == 0){
                    result=o1.getNome().compareTo(o2.getNome());
                }
                return result;
            }
        };
        
         Comparator<Contribuinte> comparatorNome_Tipo = new Comparator<Contribuinte>() {
            @Override
            public int compare(Contribuinte o1, Contribuinte o2) {
                String tipo1 = o1.getClass().getName();
                String tipo2 = o2.getClass().getName();
                return tipo1.compareTo(tipo2);
            }
        };
        
        Collections.sort(contribuintes,comparatorTipo);
        System.out.println("");
        for (int i = 0; i < contribuintes.size(); i++) {
            System.out.println(contribuintes.get(i));
        }
        
        Collections.sort(contribuintes,comparatorNome_Tipo);
        System.out.println("");
        for (int i = 0; i < contribuintes.size(); i++) {
            System.out.println(contribuintes.get(i));
        }

    }
}
