/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author johns
 */
public class ContadorEletricidadeBiHorarioTest {
    
    public ContadorEletricidadeBiHorarioTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getIdentificador method, of class ContadorEletricidadeBiHorario.
     */
    @Test
    public void testGetIdentificador() {
        System.out.println("getIdentificador");
        ContadorEletricidadeBiHorario instance = new ContadorEletricidadeBiHorario(220, 220);
        String expResult = "ELET-1";
        String result = instance.getIdentificador();
        assertEquals(expResult, result);
    }

    /**
     * Test of calcularCusto method, of class ContadorEletricidadeBiHorario.
     */
    @Test
    public void testCalcularCusto() {
        System.out.println("calcularCusto");
        ContadorEletricidadeBiHorario instance = new ContadorEletricidadeBiHorario(23, 32);
        double expResult = 3.22 + 2.112;
        double result = instance.calcularCusto();
        assertEquals(expResult, result,3.22+2.112);
    }
    
}
