/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Ignore;
import static org.junit.Assert.*;

/**
 *
 * @author johns
 */
public class ContadorGasTest {
    
    public ContadorGasTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of changeCustoM3 method, of class ContadorGas.
     */
    @Ignore
    public void testChangeCustoM3() {
        System.out.println("changeCustoM3");
        float custoM3 = 0.0F;
        ContadorGas.changeCustoM3(custoM3);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getIdentificador method, of class ContadorGas.
     */
    @Test
    public void testGetIdentificador() {
        System.out.println("getIdentificador");
        ContadorGas instance = new ContadorGas();
        String expResult = "GAS-1";
        String result = instance.getIdentificador();
        assertEquals(expResult, result);
    }

    /**
     * Test of calcularCusto method, of class ContadorGas.
     */
    @Test
    public void testCalcularCusto() {
        System.out.println("calcularCusto");
        ContadorGas instance = new ContadorGas(10);
        double expResult = 8.0;
        double result = instance.calcularCusto();
        assertEquals(expResult, result, 8.0);
        
    }
    
}
