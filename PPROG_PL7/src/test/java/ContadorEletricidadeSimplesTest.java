/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author johns
 */
public class ContadorEletricidadeSimplesTest {
    
    public ContadorEletricidadeSimplesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calcularCusto method, of class ContadorEletricidadeSimples.
     */
    @Test
    public void testCalcularCusto() {
        System.out.println("calcularCusto");
        ContadorEletricidadeSimples instance = new ContadorEletricidadeSimples(6.9);
        double expResult = 1.104;
        double result = instance.calcularCusto();
        assertEquals(expResult, result, 1.104);
    }
    @Test
    public void testCalcularCusto2() {
        System.out.println("calcularCusto");
        ContadorEletricidadeSimples instance = new ContadorEletricidadeSimples(5);
        double expResult = 0.8;
        double result = instance.calcularCusto();
        assertEquals(expResult, result, 0.8);
    }
    
}
