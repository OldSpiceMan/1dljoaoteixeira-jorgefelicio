public class ContadorGas extends Contador {

	private static double custoM3 = 0.8;
	private final String IdentificadorGas = "GAS";
        private static int numContadoresDeGas = 1;

        public ContadorGas(){
            super();
            numContadoresDeGas++;
        }
        
        public ContadorGas(int consumoMes){
            super(consumoMes, "GAS-" + numContadoresDeGas);
            numContadoresDeGas++;
        }
        
        public static void changeCustoM3 (float custoM3){
            ContadorGas.custoM3 = custoM3;
        }
        
        public String getIdentificador(){
            return String.format("%s" + "-" + "%s", IdentificadorGas, numContadoresDeGas );
        }
        
        public double calcularCusto (){
            return custoM3 * super.getConsumo();
        }
        
            
}