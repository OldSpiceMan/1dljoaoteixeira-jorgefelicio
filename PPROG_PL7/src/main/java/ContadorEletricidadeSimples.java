
public class ContadorEletricidadeSimples extends ContadorEletricidade {

    private double custoTarifarioSimplesPotenciaBaixa = 0.13;
    private double custoTarifarioSimplesCPotenciaAlta = 0.16;
    private double potenciaContratada;
    private final double POTENCIA_CONTRATADA_POR_OMISSAO = 0;
    private final double VALOR_MARGEM_POTENCIA_CONTRATADA = 6.9;

    public ContadorEletricidadeSimples(int consumoPorMes) {
        super(consumoPorMes);
        this.potenciaContratada = POTENCIA_CONTRATADA_POR_OMISSAO;
    }

    public double getPotenciaContratada() {
        return potenciaContratada;
    }

    public ContadorEletricidadeSimples(double potencia) {
        super();
        this.potenciaContratada = potencia;
    }

    public double calcularCusto() {
        if (VALOR_MARGEM_POTENCIA_CONTRATADA > potenciaContratada) {
            return custoTarifarioSimplesPotenciaBaixa * super.getConsumo();
        } else {
            return custoTarifarioSimplesCPotenciaAlta * super.getConsumo();
        }
    }

}
