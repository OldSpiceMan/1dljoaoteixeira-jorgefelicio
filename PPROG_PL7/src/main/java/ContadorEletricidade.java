
public abstract class ContadorEletricidade extends Contador {

    private static String identificadorEletricidade = "ELET";
    private static int numContadoresDeElet = 1;

    public ContadorEletricidade() {
        super();
        numContadoresDeElet++;
        
    }

    public ContadorEletricidade(int consumoPorMes) {
        super(consumoPorMes, "ELET-" + Integer.toString(numContadoresDeElet) );
        numContadoresDeElet++;
    }

    public int getConsumo() {
        return super.getConsumo();
    }

    public String getIdentificador() {
        return String.format("%s" + "-" + "%s", identificadorEletricidade, numContadoresDeElet);
    }

    public String getIdentificadorEletrico() {
        return identificadorEletricidade;
    }

    public String getNumeroContadoresEletricos() {
        return Integer.toString(numContadoresDeElet);
    }
    
    public static int obtainNumeroContadoresEletricos(){
        return numContadoresDeElet;
    }

    public abstract double calcularCusto();
}
