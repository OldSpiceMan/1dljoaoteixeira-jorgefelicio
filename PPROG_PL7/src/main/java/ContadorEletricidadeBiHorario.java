public class ContadorEletricidadeBiHorario extends ContadorEletricidade {

	private final double CUSTOTARIFARIOHORASVAZIO = 0.066;
	private final double CUSTOTARIFARIOHORASFORADOVAZIO = 0.14;
        private int consumoHorasForaDeVazio;
        private int consumoHorasVazio;
        private final int CONSUMOHORASFORAVAZIO_POR_OMISSAO = 0;
        private final int CONSUMOHORASVAZIO_POR_OMISSAO = 0;

        
        public ContadorEletricidadeBiHorario(int consumoPorMes){
            super(consumoPorMes);
            consumoHorasForaDeVazio =  CONSUMOHORASFORAVAZIO_POR_OMISSAO;
            consumoHorasVazio = CONSUMOHORASVAZIO_POR_OMISSAO;
        }
        
        public  ContadorEletricidadeBiHorario(int consumoHorasForaDeVazio, int consumoHorasVazio){
            super(consumoHorasForaDeVazio);
            this.consumoHorasForaDeVazio = consumoHorasForaDeVazio;
            this.consumoHorasVazio = consumoHorasVazio;
        }
        
        public String getIdentificador(){
            return String.format("%s" + "-" + "%s", super.getIdentificadorEletrico(), super.getNumeroContadoresEletricos());
        }
        
        @Override
        public double calcularCusto(){
         return consumoHorasForaDeVazio * CUSTOTARIFARIOHORASFORADOVAZIO + consumoHorasVazio * CUSTOTARIFARIOHORASVAZIO;
        }

}