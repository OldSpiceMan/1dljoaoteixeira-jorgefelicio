public abstract class Contador {

	private int consumoDoMes;
        private String id;
        private static int totalContadores = 0;
        private int CONSUMODOMES_POR_OMISSAO = 0;
        
        public Contador(){
            this.consumoDoMes = CONSUMODOMES_POR_OMISSAO;
            totalContadores++;
        }
        
        public Contador(int consumoDoMes, String id){
            this.consumoDoMes = consumoDoMes;
            this.id = id;
            totalContadores++;
        }
        
        public void setConsumo (int consumoDoMes){
            this.setConsumoDoMes(consumoDoMes);
        }
        
        public int getConsumo(){
            return getConsumoDoMes();
        }
        
        public int getTotalContadores(){
            return totalContadores;
        }
        
        public String toString(){
            return id;
        }
        
        public abstract double calcularCusto();

    /**
     * @return the consumoDoMes
     */
    public int getConsumoDoMes() {
        return consumoDoMes;
    }

    /**
     * @param consumoDoMes the consumoDoMes to set
     */
    public void setConsumoDoMes(int consumoDoMes) {
        this.consumoDoMes = consumoDoMes;
    }

    /**
     * @param aTotalContadores the totalContadores to set
     */
    public static void setTotalContadores(int aTotalContadores) {
        totalContadores = aTotalContadores;
    }

    /**
     * @return the CONSUMODOMES_POR_OMISSAO
     */
    public int getCONSUMODOMES_POR_OMISSAO() {
        return CONSUMODOMES_POR_OMISSAO;
    }

    /**
     * @param CONSUMODOMES_POR_OMISSAO the CONSUMODOMES_POR_OMISSAO to set
     */
    public void setCONSUMODOMES_POR_OMISSAO(int CONSUMODOMES_POR_OMISSAO) {
        this.CONSUMODOMES_POR_OMISSAO = CONSUMODOMES_POR_OMISSAO;
    }
        
        
        
}