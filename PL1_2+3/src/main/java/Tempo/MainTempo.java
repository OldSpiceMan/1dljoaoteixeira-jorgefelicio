/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tempo;

/**
 *
 * @author Jorge Felicio
 */
public class MainTempo {
    public static void main(String[] args) {
        Tempo T1 = new Tempo("11:35:54");
        
        System.out.println(T1.toHourMinuteSecondAMorPMString());
        T1.add1Second();
        System.out.println(T1.toHourMinuteSecondAMorPMString());
        Tempo T2 = new Tempo(18,5,20);
        System.out.println(T2.toHourMinuteSecondAMorPMString());
        System.out.println(T1.maiorTempo(T2));
        System.out.println(T1.maiorTempoVariaveis(23, 7, 4));
        System.out.println(T1.diferencaTempo(T2));
        System.out.println(T1.diferencaSegundos(T2));
        System.out.println(Tempo.diferencaTempo(T1, T2));
    }
}