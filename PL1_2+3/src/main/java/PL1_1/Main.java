/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PL1_1;

import java.util.Scanner;

/**
 *
 * @author Jorge Felicio
 */
public class Main {
    public static void main (String[] args){
        Pessoa pessoa = new Pessoa();
        Scanner sc = new Scanner(System.in);
        System.out.println("Qual e o seu nome?");
        String nome= sc.nextLine();
        System.out.println("Qual é a sua idade?");
        int idade =sc.nextInt();
        pessoa.setIdade(idade);
        pessoa.setName(nome);
        System.out.println(pessoa.toString());
    }
}
