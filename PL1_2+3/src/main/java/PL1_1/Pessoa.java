/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PL1_1;

/**
 *
 * @author Jorge Felicio
 */
public class Pessoa {
    private String nome;
    private int idade;
    
    private static final String NOME_POR_OMISSAO="Sem Nome";
    
    public Pessoa(String nome,int idade){
        this.nome= nome;
        this.idade= idade;
    }
    
    public Pessoa(String nome){
        this.nome = nome;
    }
    
    public Pessoa(){
        this.nome = NOME_POR_OMISSAO;
    }
    
    public String getName(){
        return nome;
    }
    
    public int getIdade(){
        return idade;
    }
    
    public void setName(String nome){
        this.nome = nome;
    }
    
    public void setIdade(int idade){
        this.idade = idade;
    }
    
    public String toString() {
        return String.format("%s tem %s anos", nome,idade);
    }
}
