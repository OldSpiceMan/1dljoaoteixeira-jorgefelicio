/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ExercicioRetangulos;

/**
 *
 * @author johns
 */
public class Retangulo {

    private final Point p1;
    private final Point p2;
    private final Point p3;
    private final Point p4;

    public Retangulo(Point p1, Point p2, Point p3, Point p4) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
    }

    public boolean isRectangle() {

        if (p1.getX() == p2.getX() && p3.getX() == p4.getX() && p1.getX() != p3.getX() && p1.getY() == p4.getY() && p2.getY() == p3.getY() && p2.getY() != p1.getY()) {
            return true;
        }
        return false;
    }

    public boolean inside(Retangulo outroRetangulo) {
        if (p1.getX() < outroRetangulo.p1.getX() && p1.getY() < outroRetangulo.p1.getY() && p2.getX() < outroRetangulo.p2.getX() && p2.getY() > outroRetangulo.p2.getY() &&
                p3.getX() > outroRetangulo.p3.getX() && p3.getY() > outroRetangulo.p3.getY() && p4.getX() > outroRetangulo.p4.getX() && p4.getY() < outroRetangulo.p4.getY()) {
            return true;
        }
        return false;

    }

    public boolean outside(Retangulo outroRetangulo) {
        if ((p1.getX() > outroRetangulo.p1.getY() && p1.getX() > outroRetangulo.p2.getX() && p1.getX() > outroRetangulo.p2.getX() && p1.getX() > outroRetangulo.p1.getX())||
                (p4.getX() < outroRetangulo.p1.getX() && p4.getX() < outroRetangulo.p4.getX() && p4.getX() < outroRetangulo.p2.getX() && p4.getX() < outroRetangulo.p3.getX())||
                (p1.getY() > outroRetangulo.p1.getY() && p1.getY() > outroRetangulo.p2.getY() && p1.getY() > outroRetangulo.p3.getY() && p1.getY() > outroRetangulo.p4.getY())||
                (p2.getY()< outroRetangulo.p1.getY() && p2.getY()< outroRetangulo.p2.getY() && p2.getY()< outroRetangulo.p3.getY() && p2.getY()< outroRetangulo.p4.getY())){
            return true;
        }
        return false;
    }

    public boolean intersect(Retangulo outroRetangulo) {
        if (outroRetangulo.p1.getX() > p1.getX() && outroRetangulo.p1.getX() < p4.getX() && outroRetangulo.p1.getY() < p3.getY() && outroRetangulo.p1.getY() > p1.getY() && outroRetangulo.p2.getY() > p2.getY()) {
            return true;
        }
        return false;
    }

    public Retangulo retornarRetanguloIntersecao(Retangulo outroRetangulo) {
        Point p1RetanguloInters = new Point(outroRetangulo.p1.getX(), outroRetangulo.p1.getY());
        Point p2RetanguloInters = new Point(outroRetangulo.p1.getX(), p2.getY());
        Point p3RetanguloInters = new Point(p4.getX(), p4.getY());
        Point p4RetanguloInters = new Point(p4.getX(), outroRetangulo.p1.getY());
        return new Retangulo(p1RetanguloInters, p2RetanguloInters, p3RetanguloInters, p4RetanguloInters);
    }
    @Override
    public String toString(){
    return String.format("O Retangulo tem como pontos %s %s %s %s", p1.getPoint(), p2.getPoint(), p3.getPoint(), p4.getPoint());
    }
}
