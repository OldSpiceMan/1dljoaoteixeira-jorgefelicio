/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ExercicioRetangulos;

/**
 *
 * @author johns
 */
public class Point {
    private int x;
    private int y;
    public Point(){
        x = 0;
        y = 0;
    }
    
    public Point (int x, int y){
        this.x = x;
        this.y = y;
    }
    
    public int getX(){
        return x;
    }
    public int getY (){
        return y;
    }
    
    public int getDistanceX (Point outroPonto){
        return x-outroPonto.x;
    }
    public int getDistanceY (Point outroPonto){
        return y-outroPonto.y;
    }
    
    public String getPoint(){
        return ("(" + x + ","+ y +")");
    }
    
   




 
}
