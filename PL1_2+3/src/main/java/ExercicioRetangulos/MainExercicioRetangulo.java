/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ExercicioRetangulos;

/**
 *
 * @author johns
 */
public class MainExercicioRetangulo {

    public static void main(String[] args) {
        Point p1 = new Point(0, 0);
        Point p2 = new Point(0, 5);
        Point p3 = new Point(5, 5);
        Point p4 = new Point(5, 0);
        Retangulo R1 = new Retangulo(p1,p2,p3,p4);
        System.out.println(R1.isRectangle());
        Point P1= new Point(1,4);
        Point P2= new Point(1,8);
        Point P3= new Point(4,8);
        Point P4= new Point(4,4);
        Retangulo R2 = new Retangulo(P1,P2,P3,P4);
        System.out.println(R2.isRectangle());
        System.out.println(R1.inside(R2));
        System.out.println(R1.outside(R2));
        System.out.println(R1.intersect(R2));
        Retangulo R3;
        R3 = R1.retornarRetanguloIntersecao(R2);
        System.out.println(R3.toString());
    }
}