/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PL4;

/**
 *
 * @author Jorge Felicio
 */
public class Utente {

    private String nome;

    private String genero;

    private int idade;

    private double altura;

    private double peso;

    private String NOME_POR_OMISSAO = "Sem Nome";

    private String GENERO_POR_OMISSAO = "Sem Genero";

    private int IDADE_POR_OMISSAO = 0;

    private double ALTURA_POR_OMISSAO = 0;

    private double PESO_POR_OMISSAO = 0;

    private int IMC_MAGRO = 18;

    private int IMC_OBESO = 25;

    private static int counter = 0;

    Utente() {
        this.nome = NOME_POR_OMISSAO;
        this.genero = GENERO_POR_OMISSAO;
        this.idade = IDADE_POR_OMISSAO;
        this.altura = ALTURA_POR_OMISSAO;
        this.peso = PESO_POR_OMISSAO;
        this.counter++;
    }

    Utente(String nome, int idade) {
        this.nome = nome;
        this.idade = idade;
        this.counter++;

    }


    Utente(String nome, String genero, int idade, double altura, double peso) {
        this.nome = nome;
        this.genero = genero;
        this.idade = idade;
        this.altura = altura;
        this.peso = peso;
       
        this.counter++;
    }

    public String getNome() {
        return nome;
    }

    public String getGenero() {
        return genero;
    }

    public int getIdade() {
        return idade;
    }

    public double getAltura() {
        return altura;
    }

    public double getPeso() {
        return peso;
    }

    public int getCounter() {
        return counter;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public String toString() {
        return String.format("%s é do sexo %s, com %s anos, %s de altura e com o peso de %s", nome, genero, idade, altura, peso);
    }

    public int diferencaIdade(Utente outroUtente) {
        int diferencaIdade = this.idade - outroUtente.getIdade();
        if (diferencaIdade < 0) {
            diferencaIdade = -diferencaIdade;
        }
        return diferencaIdade;
    }

    public double calculoIMC() {
        double IMC = peso / Math.pow(altura, 2);
        return IMC;
    }

    public String classificacaoIMC() {
        double IMC = calculoIMC();
        String classificacao;
        if (IMC < this.IMC_MAGRO) {
            classificacao = "magro";
        } else if (IMC > this.IMC_OBESO) {
            classificacao = "obeso";
        } else {
            classificacao = "Saudavel";
        }
        return classificacao;
    }

    public Boolean verificacaoSaudavel() {
        String saudavel = classificacaoIMC();
        if (saudavel.equals("Saudavel")) {
            return true;
        }
        return false;
    }

    public String isMaior(Utente outroUtente) {
        String utenteMaior;
        int maior = this.idade - outroUtente.getIdade();
        if (maior < 0) {
            utenteMaior = this.nome;
        }
        if (maior > 0) {
            utenteMaior = outroUtente.getNome();
        } else {
            utenteMaior = "mesma idade";
        }
        return utenteMaior;
    }
}
