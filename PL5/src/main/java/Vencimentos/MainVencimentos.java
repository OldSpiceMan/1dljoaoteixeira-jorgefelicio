/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vencimentos;

/**
 *
 * @author Jorge Felicio
 */
public class MainVencimentos {

    public static void main(String[] args) {
        TrabalhadorPeca jorge = new TrabalhadorPeca("Jorge Silva", 53, 62);
        TrabalhadorComissao susana = new TrabalhadorComissao("Susana Ferreira", 650.00f, 4.25f, 40.50f);
        TrabalhadorHora carlos = new TrabalhadorHora("Carlos Miguel", 168, 4.50f);

        Trabalhador[] TrabalhadorList = new Trabalhador[10];
        TrabalhadorList[0] = jorge;
        TrabalhadorList[1] = susana;
        TrabalhadorList[2] = carlos;
        int nElems = 3;
        
        for(int i = 0; i < nElems; i++){
            System.out.println(TrabalhadorList[i].getNome());
        }
        for (int i = 0; i < nElems; i++) {
            if (TrabalhadorList[i] instanceof TrabalhadorHora) {
                System.out.println(TrabalhadorList[i].getNome());
            }
        }
        for (int i = 0; i < nElems; i++) {
            System.out.print(((Trabalhador)TrabalhadorList[i]).getNome());
            System.out.print(" ");
            System.out.println(String.format("%.2f%n", TrabalhadorList[i].calcularVencimento()));
        }
    }
}
