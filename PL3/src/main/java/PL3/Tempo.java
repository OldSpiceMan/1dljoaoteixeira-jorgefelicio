/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PL3;

/**
 *
 * @author Jorge Felicio
 */
public class Tempo {
    private int hh;
    
    private int mm;
    
    private int ss;
    
    private static final int HH_POR_OMISSAO = 0;
    
    private static final int MM_POR_OMISSAO = 0;
    
    private static final int SS_POR_OMISSAO =0;
    
    /**
     * Constroi uma instancia da classe Tempo a partir de inteiros.
     * @param hh Hora inserida pelo utilizador
     * @param mm Minuto inseirido pelo utilizador
     * @param ss Segundo inserido pelo utilizador.
     */
    
    public Tempo(int hh,int mm, int ss){
        this.hh=hh;
        this.mm=mm;
        this.ss=ss;
    }
    /**
     * Constrói uma instancia da classe Tempo por omissão.
     */
    
    public Tempo(){
        this.hh=HH_POR_OMISSAO;
        this.mm=MM_POR_OMISSAO;
        this.ss=SS_POR_OMISSAO;
    }
    
    /**
     * Constrói uma instancia da classe Tempo a partir de uma String.
     * @param hh_mm_ss Tempo inserido pelo utilizador na forma Hora_Minuto_Segundo.
     */
    public Tempo(String hh_mm_ss){
        String[] tempo = hh_mm_ss.split(":");
        this.hh = Integer.parseInt(tempo[0]);
        this.mm = Integer.parseInt(tempo[1]);
        this.ss = Integer.parseInt(tempo[2]);
    }
    
    /**
     * Método que obtém a hora a partir de um objeto da classe Tempo.
     * @return Hora do objeto da classe Tempo.
     */
    public int getHH(){
        return hh;
    }
    /**
     * Método que obtém o minuto a partir de um objeto da classe Tempo.
     * @return Minuto do objeto da classe Tempo.
     */
    public int getMM(){
        return mm;
    }
    
    /**
     * Método que obtém o segundo a partir de um objeto da classe Tempo.
     * @return Segundo do objeto da classe Tempo.
     */
    public int getSS(){
        return ss;
    }
    /**
     * Método que permite ao utilizador estabelecer um novo tempo num objeto da classe Tempo.
     * @param hh Nova Hora inserida do objeto da classe Tempo.
     * @param mm Novo Minuto inserido do objeto da classe Tempo.
     * @param ss Novo Segundo inserido do objeto da classe Tempo.
     */
    public void setTempo(int hh,int mm,int ss){
        this.hh=hh;
        this.mm=mm;
        this.ss=ss;
    }
    /**
     * Metodo que da o tempo no formato HH:MM:SS
     * @return Tempo de uma instância da classe Tempo no formato HH:MM:SS.
     */
    public String toHourMinuteSecondString() {
        return hh + ":" + mm + ":" + ss;
    }
    /**
     * Metodo que da o tempo no formato HH:MM:SS AM or PM
     * @return Tempo de uma instância da classe Tempo no formato HH:MM:SS AM or PM.
     */
    public String toHourMinuteSecondAMorPMString(){
        if(hh< 24 & hh > 12){
            int temphh = hh-12;
            return temphh + ":" + mm +":" + ss + "PM";
        }
        else if( hh<=12){
            return hh + ":" + mm + ":" + ss + "AM";
        }
        return "Valores invalidos";
    }
    /**
     * Método que adiciona um segundo a uma instancia da classe Tempo.
     */
    public void add1Second(){
        if(this.ss == 59){
            this.ss=0;
            if(this.mm == 59){
                this.mm=0;
                this.hh=hh+1;
            }
            else{
                this.mm=mm+1;
            }
        }
        else{
            this.ss=ss+1;
         
        }
    }
    /**
     * Método que compara o Tempo, recebendo um objeto da classe Tempo como parametro
     * @param outroTempo Tempo inserido pelo utilizador para a comparação
     * @return true se o objeto inicial é maior que o objeto de comparação, false se o objeto inicial é menor que o objeto de comparação.
     */
    public boolean maiorTempo(Tempo outroTempo){
        if(hh>outroTempo.hh){
            return true;
        }
        else if(hh == outroTempo.hh && mm>outroTempo.mm){
            return true;
        }
        else if(hh == outroTempo.hh && mm == outroTempo.mm && ss>outroTempo.ss){
            return true;
        }
        return false;
    }
    /**
     * Método que compara o Tempo, recebendo um tempo definido pelo utilizador como parametro.
     * @param hh Hora inserida pelo utilizador para o tempo.
     * @param mm Minuto inserido pelo utilizador para o tempo.
     * @param ss Segundo inserido pelo utilizador para o tempo.
     * @return true se o objeto inicial é maior que o tempo de comparação, false se o objeto inicial é menor que o tempo de comparação.
     */
    public boolean maiorTempoVariaveis(int hh,int mm,int ss){
        if(this.hh > hh){
            return true;
        }
        else if(this.hh == hh && this.mm >mm){
            return true;
        }
        else if(this.hh == hh && this.mm == mm && this.ss > ss){
            return true;
        }
        return false;
        
    }
    /**
     * Método que faz a diferença entre dois objetos da classe Tempo.
     * @param outroTempo objeto utilizado como comparação.
     * @return Diferença entre os tempos.
     */
    public Tempo diferencaTempo(Tempo outroTempo){
        if(maiorTempo(outroTempo)== true){
            int hhdiff = hh-outroTempo.hh;
            int mmdiff = mm-outroTempo.mm;
            int ssdiff = ss-outroTempo.ss;
            if(mmdiff <0){
                mmdiff=60+mmdiff;
                hhdiff=hhdiff-1;
            }
            if(ssdiff<0){
                ssdiff= 60+ssdiff;
                mmdiff=mmdiff-1;
                if(mmdiff<0){
                    mmdiff=60+mmdiff;
                    hhdiff=hhdiff-1;
                }
            }
            Tempo tempoDiff = new Tempo(hhdiff, mmdiff, ssdiff);
            return tempoDiff;
        }
        int hhdiff = outroTempo.hh-hh;
        int mmdiff = outroTempo.mm-mm;
        int ssdiff = outroTempo.ss-ss;
         if(mmdiff <0){
                mmdiff=60+mmdiff;
                hhdiff=hhdiff-1;
            }
            if(ssdiff<0){
                ssdiff= 60+ssdiff;
                mmdiff=mmdiff-1;
                if(mmdiff<0){
                    mmdiff=60+mmdiff;
                    hhdiff=hhdiff-1;
                }
            }
        Tempo tempoDiff = new Tempo (hhdiff, mmdiff, ssdiff);
        return tempoDiff;
    }
    /**
     * Diferença entre dois tempos, ambos inseridos pelo utilizador, em segundos. 
     * @param tempo
     * @param outroTempo
     * @return 
     */
    
    public static int diferencaTempo(Tempo tempo,Tempo outroTempo){
        if(tempo.maiorTempo(outroTempo)== true){
            int hhdiff = tempo.hh-outroTempo.hh;
            int mmdiff = tempo.mm-outroTempo.mm;
            int ssdiff = tempo.ss-outroTempo.ss;
            if(mmdiff <0){
                mmdiff=60+mmdiff;
                hhdiff=hhdiff-1;
            }
            if(ssdiff<0){
                ssdiff= 60+ssdiff;
                mmdiff=mmdiff-1;
                if(mmdiff<0){
                    mmdiff=60+mmdiff;
                    hhdiff=hhdiff-1;
                }
            }
            return Math.abs(hhdiff * 3600 + mmdiff * 60 + ssdiff);
        }
        int hhdiff = outroTempo.hh-tempo.hh;
        int mmdiff = outroTempo.mm-tempo.mm;
        int ssdiff = outroTempo.ss-tempo.ss;
         if(mmdiff <0){
                mmdiff=60+mmdiff;
                hhdiff=hhdiff-1;
            }
            if(ssdiff<0){
                ssdiff= 60+ssdiff;
                mmdiff=mmdiff-1;
                if(mmdiff<0){
                    mmdiff=60+mmdiff;
                    hhdiff=hhdiff-1;
                }
            }
        
        return Math.abs(hhdiff * 3600 + mmdiff * 60 + ssdiff);
    }
    /**
     * Retorna a diferença em segundos entre dois objetos da classe Tempo
     * @param outroTempo
     * @return diferenca em segundos.
     */
    public int diferencaSegundos(Tempo outroTempo){
        Tempo diffTempo= diferencaTempo(outroTempo);
        int ssdiff = diffTempo.ss;
        int mmdiff = diffTempo.mm;
        ssdiff = ssdiff+ (mmdiff*60);
        int hhdiff = diffTempo.hh;
        ssdiff = ssdiff + (hhdiff*3600);
        return Math.abs(ssdiff);
    }
}
