/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PL1_2;

/**
 *
 * @author johns
 */
public class MainData {
    public static void main(String[] args) {
        Data data1 = new Data(2019, 02, 20);
        System.out.println(data1.toString());
        Data data2 = new Data (1974, 04, 25);
        System.out.println(data2.toAnoMesDiaString());
        if(data1.isMaior(data2)){
            System.out.println(data1.toAnoMesDiaString() + " é maior que " + data2.toAnoMesDiaString());
        }
        System.out.println("O numero de dias de diferença entre as datas é "  + String.valueOf(data1.calcularDiferenca(data2)));
        System.out.println("O numero de dias que faltam ate ao Natal é " + String.valueOf(data1.calcularDiferenca(2019, 12, 25)));
        System.out.println(data2.determinarDiaDaSemana());
        if (data2.isAnoBissexto(data2.getAno()) && Data.isAnoBissexto(data2.getAno()) ) {
            System.out.println("O ano " + data2.getAno() + " é bissexto");
        } else {
        System.out.println("O ano " + data2.getAno() + " é um ano comum");
    }
    }
   }
