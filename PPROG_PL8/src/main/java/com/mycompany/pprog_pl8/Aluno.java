/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pprog_pl8;

/**
 *
 * @author Jorge Felicio
 */
public abstract class Aluno extends Escola {

    private int idMecanografico;
    private int ID_MECANOGRAFICO_POR_OMISSAO = 0;

    public Aluno(String nome, int idCivil,int idMecanografico) {
        super(nome, idCivil);
        this.idMecanografico = idMecanografico;
    }
    
    public Aluno(){
        super();
        this.idMecanografico=ID_MECANOGRAFICO_POR_OMISSAO;
    }
    
    public void setIdMecanografico(int idMecanografico){
        this.idMecanografico=idMecanografico;
    }
    
    public int getIdMecanografico(){
        return this.idMecanografico;
    }
    
    public String toString(){
        return super.toString() + String.format(", id Mecanografico: %s ",idMecanografico);
    }
    
 
}
