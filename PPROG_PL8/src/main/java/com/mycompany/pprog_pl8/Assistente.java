/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pprog_pl8;

/**
 *
 * @author Jorge Felicio
 */
public class Assistente extends Professor{

    /**
     *
     * @param nome
     * @param idCivil
     * @param salBase
     */
    public Assistente(String nome, int idCivil) {
        super(nome,idCivil);
    }
    
    public Assistente(){
        super();
    }
    
   
    
    public float calculoSalario(){
        return super.getSalBase();
    }
    
    public String toString(){
        return super.toString()+ String.format(", Salario total: %s",calculoSalario());
    }
}
