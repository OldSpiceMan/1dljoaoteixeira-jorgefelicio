/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pprog_pl8;

/**
 *
 * @author Jorge Felicio
 */
public class Coordenador extends Professor{

    private float valMajoracaoCoordenador = (float) 0.5;
    private float VAL_MAJORACAO_COORDENADOR_POR_OMISSAO = 0;

    /**
     *
     * @param nome
     * @param idCivil
     * @param salBase
     * @param valMajoracaoCoordenador
     */
    public Coordenador(String nome, int idCivil) {
        super(nome,idCivil);
    }
    
    public Coordenador(){
        super();
        this.valMajoracaoCoordenador=VAL_MAJORACAO_COORDENADOR_POR_OMISSAO;
    }
    
 
    
    public void setValMajoracaoCoordenador(float valMajoracaoCoordenador){
        this.valMajoracaoCoordenador=valMajoracaoCoordenador;
    }
    
    public double getValMajoracaoCoordenador(){
        return this.valMajoracaoCoordenador;
    }
    
    public float calculoSalario(){
        return super.getSalBase() + super.getSalBase()*this.valMajoracaoCoordenador;
    }
    
    public String toString(){
        return super.toString() + String.format(", valor Majoracao: %s , Salario total: %s",valMajoracaoCoordenador, calculoSalario());
    }
}
