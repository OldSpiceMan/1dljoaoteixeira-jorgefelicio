/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pprog_pl8;

/**
 *
 * @author Jorge Felicio
 */
public class AlunoBolseiro extends Aluno{

    private int bolsa;
    private int BOLSA_POR_OMISSAO = 0;

    /**
     *
     * @param nome
     * @param idCivil
     * @param idMecanografico
     * @param bolsa
     */
    public AlunoBolseiro(String nome, int idCivil, int idMecanografico, int bolsa) {
        super(nome,idCivil,idMecanografico);
        this.bolsa = bolsa;
    }
    
    public AlunoBolseiro(){
        super();
        this.bolsa=BOLSA_POR_OMISSAO;
    }
    
   
    public void setBolsa(int bolsa){
        this.bolsa = bolsa;
    }
    
    public int getBolsa(){
       return this.bolsa; 
    }
    
    public String toString(){
        return super.toString() + String.format(", bolsa: %s",bolsa);
    }
}
