/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pprog_pl8;

/**
 *
 * @author Jorge Felicio
 */
public class Adjunto extends Professor{

    private float valMajoracaoAdjunto=(float) 0.2;
    
    private float VAL_MAJORACAO_ADJUNTO_POR_OMISSAO = 0;

    /**
     *
     * @param nome
     * @param idCivil
     * @param salBase
     * @param valMajoracaoAdjunto
     */
    public Adjunto(String nome, int idCivil) {
        super(nome,idCivil);
    }
    
    public Adjunto(){
        super();
        this.valMajoracaoAdjunto=VAL_MAJORACAO_ADJUNTO_POR_OMISSAO;
    }
    
 
    
    public void setValMajoracaoAdjunto(float valMajoracaoAdjunto){
        this.valMajoracaoAdjunto = valMajoracaoAdjunto;
    }
    
    public double getValMajoracaoAdjunto(){
        return this.valMajoracaoAdjunto;
    }
    
    public float calculoSalario(){
        return (float) (super.getSalBase()+ super.getSalBase()*this.getValMajoracaoAdjunto());
    }
    
    public String toString(){
        return super.toString() + String.format(", valor Majoracao: %s, Salario total: %s", getValMajoracaoAdjunto(), calculoSalario());
    }
    

}
