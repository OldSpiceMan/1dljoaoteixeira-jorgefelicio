/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pprog_pl8;

/**
 *
 * @author Jorge Felicio
 */
public class Escola {

    private String nome;
    private int idCivil;
    private int ID_CIVIL_POR_OMISSAO = 0;
    private String NOME_POR_OMISSAO = "...";

    Escola(String nome, int idCivil) {
        this.nome = nome;
        this.idCivil = idCivil;
    }
    
    Escola(){
        this.nome=NOME_POR_OMISSAO;
        this.idCivil =ID_CIVIL_POR_OMISSAO;
    }
    
    public void setNome(String nome){
        this.nome=nome;
    }
    
    public String getNome(){
        return this.nome;
    }
    
    public void setIdCivil(int idCivil){
        this.idCivil=idCivil;
    }
    
    public int getIdCivil(){
        return this.idCivil;
    }
    
    public String toString(){
        return String.format("Nome: %s, Id civil: %s", nome,idCivil);
    }
}
