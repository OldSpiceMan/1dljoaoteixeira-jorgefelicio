/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pprog_pl8;

/**
 *
 * @author Jorge Felicio
 */
public abstract class Professor extends Escola{

    private float salBase = 1500;
    private int SAL_BASE_POR_OMISSAO = 0;

    public Professor(String nome, int idCivil) {
        super(nome, idCivil);
    }
    public Professor(){
        super();
        this.salBase = SAL_BASE_POR_OMISSAO;
    }
    
    public void setSalBase(float salBase){
        this.salBase=this.salBase;
    }
    
    public float getSalBase(){
        return this.salBase;
    }
    

    
    public String toString(){
        return super.toString() + String.format(", salario base: %s",salBase);
    }
    
    public abstract float calculoSalario();
    
}
