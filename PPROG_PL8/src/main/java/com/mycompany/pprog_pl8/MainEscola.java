/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pprog_pl8;

/**
 *
 * @author Jorge Felicio
 */
public class MainEscola {
    
    public static void main(String[] args){
        Object[] obj = new Object[8];
        obj[0] = new AlunoBolseiro("Nuno",1234,1234,400);
        obj[1] = new AlunoNaoBolseiro("Jorge",12345,12345);
        obj[2] = new Adjunto("Pedro",123456);
        obj[3] = new Assistente("Miguel",1234567);
        obj[4] = new Coordenador("Wiener",123);
        obj[5] = new AlunoBolseiro();
        obj[6] = new AlunoNaoBolseiro();
        obj[7] = new Adjunto();
        int countBolseiros = 0;
        int countProfs = 0;
        int countTotal = 0;
        for(int i=0;i<obj.length;i++){
            if(obj[i] instanceof Professor){
                Escola Escola = (Escola) obj[i];
                System.out.println("Nome:" + Escola.getNome()+ ", Categoria:" + obj[i].getClass().getSimpleName());
                Professor prof = (Professor) obj[i];
                System.out.println("Salario total:" + prof.calculoSalario());
                countProfs++;
                countTotal++;
            }
            if(obj[i] instanceof AlunoNaoBolseiro){
                Aluno Aluno = (Aluno) obj[i];
                System.out.println("Id mecanografico:" + Aluno.getIdMecanografico());
            }
            if(obj[i] instanceof AlunoBolseiro){
                AlunoBolseiro Aluno = (AlunoBolseiro) obj[i];
                System.out.println(Aluno.getNome()+Aluno.getBolsa());
                countBolseiros++;
                countTotal++;
            }
            Escola Escola = (Escola) obj[i];
            System.out.println("Nome:"+ Escola.getNome()+ ", Classe:"+ obj[i].getClass().getSimpleName());
            System.out.println("");
            
        }
        System.out.println("Numero de profs:"+ countProfs + ", ´Numero de Alunos Bolseiros:" + countBolseiros + ", Numero total de encargos:" + countTotal);

    }
    
}
