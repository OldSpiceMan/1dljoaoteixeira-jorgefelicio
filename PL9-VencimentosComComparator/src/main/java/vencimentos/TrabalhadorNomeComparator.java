/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vencimentos;

import java.util.Comparator;

/**
 *
 * @author johns
 */
public class TrabalhadorNomeComparator implements Comparator<Trabalhador> {

    @Override
    public int compare(Trabalhador o1, Trabalhador o2) {
       return o1.getNome().compareToIgnoreCase(o2.getNome());
    }

}
