/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vencimentos;

import java.util.Comparator;

/**
 *
 * @author johns
 */
public class TrabalhadorSalarioComparator implements Comparator<Trabalhador> {

    @Override
    public int compare(Trabalhador o1, Trabalhador o2) {
        if (o1.calcularVencimento() < o2.calcularVencimento()) {
            return  -1;
        } else if (o1.calcularVencimento() > o2.calcularVencimento()) {
            return 1;
        }
        return 0;
    }
    
}
