/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vencimentos;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author johns
 */
public class TrabalhadorNomeComparatorTest {
    
    public TrabalhadorNomeComparatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of compare method, of class TrabalhadorNomeComparator.
     */
    @Test
    public void testCompare() {
        System.out.println("compare");
        Trabalhador o1 = new TrabalhadorComissao("Matilde", 1000, 20, 200);
        Trabalhador o2 = new TrabalhadorComissao("Maria", 1000, 20, 20);
        TrabalhadorNomeComparator instance = new TrabalhadorNomeComparator();
        int expResult = 2;
        int result = instance.compare(o1, o2);
        assertEquals(expResult, result);
    }
    
}
