/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vencimentos;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author johns
 */
public class TrabalhadorSalarioComparatorTest {
    
    public TrabalhadorSalarioComparatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of compare method, of class TrabalhadorSalarioComparator.
     */
    @Test
    public void testCompare() {
        System.out.println("compare");
        Trabalhador o1 = new TrabalhadorComissao("Rebeca", 1500, 0, 0);
        Trabalhador o2 = new TrabalhadorComissao("Nino", 2000, 0, 0);
        TrabalhadorSalarioComparator instance = new TrabalhadorSalarioComparator();
        int expResult = -1;
        int result = instance.compare(o1, o2);
        Trabalhador o3 = new TrabalhadorComissao("Rebeca", 2500, 0, 0);
        Trabalhador o4 = new TrabalhadorComissao("Rebeca", 1500, 0, 0);
        assertEquals(expResult, result);
        int expResult2 = 1;
        int result2 = instance.compare(o3, o4);
        assertEquals(expResult2, result2);
        Trabalhador o5 = new TrabalhadorComissao("Rebeca", 1500, 0, 0);
        Trabalhador o6 = new TrabalhadorComissao("Rebeca", 1500, 0, 0);
        int expResult3 = 0;
        int result3 = instance.compare(o5, o6);
        assertEquals(expResult3, result3);

    }
}
